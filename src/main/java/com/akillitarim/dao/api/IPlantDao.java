package com.akillitarim.dao.api;

import com.akillitarim.entity.Plant;

public interface IPlantDao  {

	public void savePlant(Plant plant);

	public Plant getPlantById(int pid);

	public void updatePlant(Plant plantById);
}
