package com.akillitarim.dao.api;

import com.akillitarim.entity.Environment;
import com.akillitarim.entity.User;

public interface SensorDao {
	
	public void saveMetrics(Environment metric);

	public Environment getLatestEnvironmentMetrics();

	public Environment getEnvironmentById(int pid);

	public void updateMetrics(Environment metric);

	public User getUserByUsername(String username);
}
