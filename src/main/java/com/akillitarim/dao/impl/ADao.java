package com.akillitarim.dao.impl;

import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.akillitarim.entity.ACommon;

public class ADao {
	
	
	
    @Autowired
    private SessionFactory sessionFactory;
 
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
 
    public void persist(Object entity) {
    	ACommon commonObject= (ACommon) entity;
    	if(commonObject.getEklemeZamani() == null){
    		commonObject.setEklemeZamani(new Timestamp(new Date().getTime()));
    	}
        getSession().persist(entity);
        getSession().flush();
    }
 
    public void delete(Object entity) {
        getSession().delete(entity);
    }
    
	protected Session getCurrentSession() {
		try {
		    return sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			return  sessionFactory.openSession();
		}
	}
	
	public Query createQuery(String query) {
		return getCurrentSession().createQuery(query);
	}
	public void updateModel(ACommon model) {
		Session session = getCurrentSession();
		model.setGuncellemeZamani(new Timestamp(new Date().getTime()));
		session.update(model);
		session.flush();
	}
	public <T extends ACommon> T getModelById(Class<T> clazz, int id) {
		Session session = getCurrentSession();
		Criteria crit = session.createCriteria(clazz);
		crit.add(Restrictions.eq("id", id));
		return (T) crit.uniqueResult();
	}

}
