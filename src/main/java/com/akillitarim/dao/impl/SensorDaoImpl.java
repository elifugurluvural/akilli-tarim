package com.akillitarim.dao.impl;

import org.springframework.stereotype.Repository;

import com.akillitarim.dao.api.SensorDao;
import com.akillitarim.entity.Environment;
import com.akillitarim.entity.User;

@Repository
public class SensorDaoImpl extends ADao implements SensorDao {

	@Override
	public void saveMetrics(Environment metric) {
		persist(metric);
	}

	@Override
	public Environment getLatestEnvironmentMetrics() {
		String hql= "from Environment order by eklemeZamani DESC";
		return (Environment) createQuery(hql).setMaxResults(1).uniqueResult();
	}

	@Override
	public Environment getEnvironmentById(int pid) {
		return getModelById(Environment.class, pid);
	}

	@Override
	public void updateMetrics(Environment metric) {
			updateModel(metric);
	}

	@Override
	public User getUserByUsername(String username) {
		String hql= "select p from User p where p.username=:username";
		return (User) createQuery(hql).setParameter("username", username).setMaxResults(1).uniqueResult();
	}

}
