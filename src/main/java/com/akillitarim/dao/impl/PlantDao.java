package com.akillitarim.dao.impl;

import org.springframework.stereotype.Repository;

import com.akillitarim.dao.api.IPlantDao;
import com.akillitarim.entity.Plant;

@Repository
public class PlantDao extends ADao implements IPlantDao {

	@Override
	public void savePlant(Plant plant) {
		persist(plant);
	}

	@Override
	public Plant getPlantById(int pid) {
		return getModelById(Plant.class, pid);
	}

	@Override
	public void updatePlant(Plant plantById) {
		updateModel(plantById);
	}
	
	

}
