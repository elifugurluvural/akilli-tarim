package com.akillitarim.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akillitarim.dao.api.SensorDao;
import com.akillitarim.entity.Environment;
import com.akillitarim.entity.User;

@Service
@Transactional(readOnly=true)
public class SensorService implements com.akillitarim.service.api.SensorService {

	@Autowired
	SensorDao sensorDao;
	
	@Override
	@Transactional(readOnly=false)
	public void saveMetrics(Environment metric) {
		sensorDao.saveMetrics(metric);
	}

	@Override
	public Environment getLatestEnvironmentMetrics() {
		Environment latestEnvironmentMetrics = sensorDao.getLatestEnvironmentMetrics();
		latestEnvironmentMetrics.getPlantsInBox().size();
		return latestEnvironmentMetrics;
	}

	@Override
	public Environment getEnvironmentById(int pid) {
		return sensorDao.getEnvironmentById(pid);
	}

	@Override
	@Transactional(readOnly=false)
	public void updateMetrics(Environment metric) {
		sensorDao.updateMetrics(metric);
	}

	@Override
	public User getUserByUsername(String username) {
		return sensorDao.getUserByUsername(username);
	}

}
