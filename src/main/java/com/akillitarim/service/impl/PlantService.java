package com.akillitarim.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akillitarim.dao.api.IPlantDao;
import com.akillitarim.entity.Plant;
import com.akillitarim.service.api.IPlantService;

@Service
@Transactional(readOnly=true)
public class PlantService implements IPlantService{

	@Autowired
	IPlantDao plantDao;
	
	@Override
	@Transactional(readOnly=false)
	public void addPlant(Plant plant) {
		plantDao.savePlant(plant);
	}

	@Override
	public Plant getPlantById(int pid) {
		return plantDao.getPlantById(pid);
	}

	@Override
	@Transactional(readOnly=false)
	public void updatePlant(Plant plantById) {
		plantDao.updatePlant(plantById);
	}

}
