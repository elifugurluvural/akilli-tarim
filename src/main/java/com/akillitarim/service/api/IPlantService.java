package com.akillitarim.service.api;

import com.akillitarim.entity.Plant;

public interface IPlantService {
	
	public void addPlant(Plant plant);

	public Plant getPlantById(int pid);

	public void updatePlant(Plant plantById);

}
