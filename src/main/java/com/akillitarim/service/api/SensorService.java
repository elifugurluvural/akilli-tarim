package com.akillitarim.service.api;

import com.akillitarim.entity.Environment;
import com.akillitarim.entity.User;

public interface SensorService {
	
	public void saveMetrics(Environment Environment);

	public Environment getLatestEnvironmentMetrics();

	public Environment getEnvironmentById(int pid);

	public void updateMetrics(Environment metric);

	public User getUserByUsername(String username);

}
