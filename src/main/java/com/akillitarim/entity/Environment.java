package com.akillitarim.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Environment")
public class Environment  extends ACommon{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int temperature;
	
	private int humidity;
	
	private boolean isClosureOpen;
	
	private int waterRate;
	
	private int section;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinTable(name="ENVIRONMENT_PLANT",joinColumns=@JoinColumn(name="ENVIRONMENT_ID"),inverseJoinColumns=@JoinColumn(name="PLANT_ID"))
	private Set<Plant> plantsInBox ;
	
	
	
	

	public Set<Plant> getPlantsInBox() {
		return plantsInBox;
	}

	public void setPlantsInBox(Set<Plant> plantsInBox) {
		this.plantsInBox = plantsInBox;
	}

	public int getSection() {
		return section;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public int getWaterRate() {
		return waterRate;
	}

	public void setWaterRate(int waterRate) {
		this.waterRate = waterRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public boolean getIsClosureOpen() {
		return isClosureOpen;
	}

	public void setClosureOpen(boolean isClosureOpen) {
		this.isClosureOpen = isClosureOpen;
	}


}
