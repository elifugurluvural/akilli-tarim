package com.akillitarim.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


@MappedSuperclass
public abstract class ACommon implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pid")
    private int pid;


	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	@Column 
	private Timestamp eklemeZamani;

	@Column
	private Timestamp guncellemeZamani;

	
	@Version
	private int surum;
	
	public ACommon() {
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}


	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj != null && obj.getClass().equals(this.getClass())) {
			int id = this.getPid();
			int pId = ((ACommon) obj).getPid();
			if (id >-1 ) {
				result=id==pId ? true:false;
			}
		}
		return result || super.equals(obj);
	}

	@Override
	public int hashCode() {
		Serializable id = getPid();
		return id == null ? super.hashCode() : id.hashCode();
	}

	public int getSurum() {
		return surum;
	}

	public void setSurum(int surum) {
		this.surum = surum;
	}

}
