package com.akillitarim.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

@Entity
public class Plant extends ACommon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private int growsRate;
	
	private int countOfGrowingPlant;
	
	private int section;
	
	private PlantImage plantImage;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinTable(name="ENVIRONMENT_PLANT",joinColumns=@JoinColumn(name="PLANT_ID"),inverseJoinColumns=@JoinColumn(name="ENVIRONMENT_ID"))
	private Environment environment;

	
	
	public int getGrowsRate() {
		return growsRate;
	}

	public void setGrowsRate(int growsRate) {
		this.growsRate = growsRate;
	}

	public int getSection() {
		return section;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public int getCountOfGrowingPlant() {
		return countOfGrowingPlant;
	}

	public void setCountOfGrowingPlant(int countOfGrowingPlant) {
		this.countOfGrowingPlant = countOfGrowingPlant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGrowsInDay() {
		return growsRate;
	}

	public void setGrowsInDay(int growsInDay) {
		this.growsRate = growsInDay;
	}

	public PlantImage getPlantImage() {
		return plantImage;
	}

	public void setPlantImage(PlantImage plantImage) {
		this.plantImage = plantImage;
	}
	
	

}
