package com.akillitarim.entity;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.Transient;

public class PlantImage extends ACommon{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Lob
	@Column(name="IMAGE")
	private byte[] image;
	
	@Column(name="IMAGE_NAME")
	private String imageName;
	
	@Column(name="EXTENSION",length=4)
	private String extension;
	
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}


	public  PlantImage() {
		super();
	}
	
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
