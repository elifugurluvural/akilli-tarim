package com.akillitarim.web.controller;

import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.akillitarim.entity.Environment;
import com.akillitarim.entity.Plant;
import com.akillitarim.entity.User;
import com.akillitarim.service.api.SensorService;

@Controller
public class CommonController {
	
	@Autowired
	SensorService sensorService;

	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(ModelMap model) {
		Environment environment=sensorService.getLatestEnvironmentMetrics();
		Set<Plant> plantsInBox = environment.getPlantsInBox();
		for (Plant plant : plantsInBox) {
			System.out.println(plant.getName());
		}
		model.addAttribute("environment",environment);
		return "index";

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		return "login";
	}
	
	@RequestMapping(value="/login",  method=RequestMethod.POST)
	public String loginPost(@RequestBody User user){
		User userByUsername = sensorService.getUserByUsername(user.getUsername());
		String returnValue="login";
		if(userByUsername!=null){
			if(userByUsername.getPassword().equals(user.getPassword())){
				returnValue="index";
			}
			else{
				returnValue="login";
			}
		}
		return returnValue;
	}


}