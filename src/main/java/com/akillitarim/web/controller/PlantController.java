package com.akillitarim.web.controller;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.akillitarim.entity.Environment;
import com.akillitarim.entity.Plant;
import com.akillitarim.service.api.IPlantService;
import com.akillitarim.service.api.SensorService;

@Controller
@RequestMapping(value="/plant")
public class PlantController {
	
	@Autowired
	SensorService sensorService;
	
	@Autowired
	IPlantService plantService;
	
	@GetMapping
	public String addPlantForm(){
		Environment environment = sensorService.getEnvironmentById(4);
		Plant plant = new Plant();
		plant.setEklemeZamani(new Timestamp(new Date().getTime()));
		plant.setName("Marul");
		plant.setGrowsInDay(10);
		plant.setEnvironment(environment);
		plantService.addPlant(plant);
		
		Plant dereOtu = new Plant();
		dereOtu.setEklemeZamani(new Timestamp(new Date().getTime()));
		dereOtu.setName("Dereotu");
		dereOtu.setGrowsInDay(8);
		dereOtu.setEnvironment(environment);
		plantService.addPlant(dereOtu);
		
		Plant maydonoz = new Plant();
		maydonoz.setEklemeZamani(new Timestamp(new Date().getTime()));
		maydonoz.setName("Maydonoz");
		maydonoz.setGrowsInDay(15);
		maydonoz.setEnvironment(environment);
		plantService.addPlant(maydonoz);
		
		Plant feslegen = new Plant();
		feslegen.setEklemeZamani(new Timestamp(new Date().getTime()));
		feslegen.setName("Feslegen");
		feslegen.setGrowsInDay(8);
		feslegen.setEnvironment(environment);
		plantService.addPlant(feslegen);
		
		Plant kekik = new Plant();
		kekik.setEklemeZamani(new Timestamp(new Date().getTime()));
		kekik.setName("Kekik");
		kekik.setGrowsInDay(8);
		kekik.setEnvironment(environment);
		plantService.addPlant(kekik);
		
		Plant nane = new Plant();
		nane.setEklemeZamani(new Timestamp(new Date().getTime()));
		nane.setName("Nane");
		nane.setGrowsInDay(8);
		nane.setEnvironment(environment);
		plantService.addPlant(nane);
		return "addPlant";
	}
	@PostMapping(value="growsrate")
	@ResponseBody
	public String saveGrowsRate(@RequestBody Plant plant){
		Plant plantById=plantService.getPlantById(plant.getPid());
		if(plantById!=null){
			plantById.setGrowsRate(plant.getGrowsRate());
			plantService.updatePlant(plantById);
		}
		return String.valueOf(plantById.getGrowsRate());
	}

}
