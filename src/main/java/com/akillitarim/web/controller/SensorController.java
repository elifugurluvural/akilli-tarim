package com.akillitarim.web.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.akillitarim.entity.Environment;
import com.akillitarim.entity.Plant;
import com.akillitarim.service.api.SensorService;

@Controller
@RequestMapping(value = "/sensor")
public class SensorController {

	@Autowired
	SensorService sensorService;

	@GetMapping
	public String sensorIndex() {

		System.out.println("girdi");
		Environment metric = new Environment();
		metric.setClosureOpen(false);
		metric.setHumidity(15);
		metric.setTemperature(23);

		sensorService.saveMetrics(metric);
		return "hello";
	}


	@PostMapping
	public String saveMetric(@RequestBody Environment metric) {
		Environment environment = sensorService.getEnvironmentById(metric.getPid());
		if (environment != null) {
			Set<Plant> plantsInBox = environment.getPlantsInBox();
			for (Plant plant : plantsInBox) {
				System.out.println(plant.getName());
			}
		}
		if (environment != null) {
			metric.setEklemeZamani(environment.getEklemeZamani());
			sensorService.updateMetrics(metric);
		} else {
			sensorService.saveMetrics(metric);
		}
		return "hello";
	}

	@PostMapping(value = "closure",produces = "application/json")
	@ResponseBody
	public String isClosureOpen(@RequestBody Environment environment) {
		Environment environmentById = sensorService.getEnvironmentById(environment.getPid());
		String closureStatue = "";
		if (environmentById.getIsClosureOpen()) {
			closureStatue = "Kapak Acik";
		} else {
			closureStatue = "Kapak Kapali";
		}
		return closureStatue;
	}

}
