//jQuery'nin yüklenmiş olması gerekir
netarti = {
	sui: {},
	ajax: {},
	emptyFunction: function (){},
	logged: function () {
		return USERNAME !== null && typeof USERNAME !== 'undefined' && USERNAME !== '';
	}
};

/**
 * jquery'den apartma
 * @param setting jquery ajax settigns<br>
 * type varsayılan posttur
 * örneğin
 * { 		
		url: url,
		data: data
	}
	@param params successCallback gönderilebilir, iki parameter alır, success ve xhr,
		faliureCallback gönderilebilir, iki parameter alır, success ve xhr<br>
	örneğin:
	{
		successCallback: function (respose, xhr) {
			//do sometihing
		}
	}
 */
netarti.ajax = function (settings, params) {
	settings = $.extend( {
		type: 'POST'
	}, settings );
	
	params = params || {};
	
	var failureCallback = params.failureCallback || netarti.emptyFunction;
	var successCallback = params.successCallback || netarti.emptyFunction;
	
	if (jQuery.isEmptyObject(settings)) {
		alert('netarti.ajax settings paramteres boş olamax');
		return;
	}
	if (jQuery.isEmptyObject(settings.url)) {
		alert('netarti.ajax settings.url paramteres boş olamax');
		return;
	}
	$.ajax(settings)
		.done(function(response, success, xhr) {
			if (response.success) {
				devLog("netarti.ajax success. url: " + settings.url);
				netarti.sui.showGrowl (response.messageList[0]);
			  	if (jQuery.isFunction(successCallback)) {
			  		successCallback.call(this, response, xhr);
			  	}
			} else {
				devLog("netarti.ajax failure. url: " + settings.url);
				netarti.sui.showGrowl (response.messageList[0], {failure: true});
				if (jQuery.isFunction(faliureCallback)) {
					faliureCallback.call(this, response, xhr);
			  	}
			}
		});
};

netarti.ajax.get = function (params) {
	
};

/**
 * Herhangi bir bileşeni logged olup olmama durumuna göre <br>
 * eğer kullanıcı sisteme girmişse ise true döner
 */
netarti.checkActionForLogged = function (componentSelector, returnUrl) {
	var logged = netarti.logged();
	if (logged) {
		return true;
	}
	
	var comp = $(componentSelector),
		message = 'You must logged in'; //TODO localization

	/*
	if (comp.hasClass ('must-logged-labeled')) { //önceden logged olma durumuna bakıldıysa ve eklendiyse
		return false;
	} else if (comp.hasClass('must-logged')) { //bileşen logged olmadan etkili olmayacaksa demektir
		debugger;
		$('<div class="ui left pointing red basic label">' + message + '</div>')
			.insertAfter(componentSelector + ".must-logged" );
		comp.addClass ('must-logged-labeled'); //bir daha eklememek için
		return false;
	}
	*/
	if (comp.hasClass('must-logged')) {
		$('#activityModal')
		.modal({
			closable: true,
			onHidden:function(){
				$('#activityModal').modal('hide dimmer');
			},
			onApprove: function (ele) {
				location.href = BASE_URL + '/login?' + PARAM_NAME_RURL + '=' + BASE_URL + returnUrl;
			}
		})
		.modal('show');
		
		return false;
	}
	
	return true;
}

/**
 * @param selected "form .button" gibi
 * @param options url ya da action zorunlu, 
 * 	 örneğin: {
 * 		url: '/xxx/submiturl',
 * 		beforeSendCallback: function (settings),
 * 		onSuccessCallback: function (response)
 * 	 }
 *  eğer options.fields boş değilse, form validation yapmaya çalışır, bu durumda options.fs gönderilmeli<br>
 *  eğer success'te bir url'ye redirect edilecekse successRedirectUrl özelliğini set etmelisiniz
 */
netarti.sui.api = function (options) {
	var beforeSendCallback = options.beforeSendCallback || netarti.emptyFunction;
	var onSuccessCallback = options.onSuccessCallback || netarti.emptyFunction;
	var settings = $.extend( {}, 
			netarti.sui.api.defaults(beforeSendCallback, onSuccessCallback, options.successRedirectUrl, options.showGeneralMessage), 
			options );
	//delete settings.beforeSendCallback;
	//delete settings.onSuccessCallback;
	if (settings.fields && settings.fs) {
		$(settings.fs).form({fields: options.fields,
			inline : true,
		    on     : 'blur'});
	}
	
	$(options.bs).api(settings);
}
netarti.sui.api.defaults = function (beforeSendCallback, onSuccessCallback, successRedirectUrl, showGeneralMessage) {
	return {
	  	//action: 'add sorun', // ya da url set edilmeli
	  	//url: BASE_URL + '/secure/sorun/',
	  	serializeObject: true,
	    contentType: "application/json; charset=utf-8",
	  	method : 'POST',
	  	beforeSend: function (settings) {
	  		if (!netarti.checkActionForLogged (settings.bs)) {
	  			return false;
	  		}
	  		devLog ('beforesend');
	  		if (settings.fields && settings.fs) {
		  		if (!$(settings.fs).form('validate').form()) {
		  			 $('.ui.error.message').removeClass('hidden');
		  			return false; //TODO yeni sürümde düzelmiş, şimdilik hatalı kesiyor
		  		}
	  		}
		  	$('.message').removeClass('success').removeClass('negative').addClass('hidden');
		  	if (false === settings.contentType || 'multipart/form-data' === settings.contentType) {
		  		if (!settings.fs) {
		  			alert('fs form selector giriniz');
		  			return false;
		  		}
		  		settings.data = new FormData($(settings.fs)[0]);
		  	} else if (settings.fs){
		  		settings.data = JSON.stringify($(settings.fs).form('get values'));
		  	}
		  	if (jQuery.isFunction(beforeSendCallback)) {
		  		beforeSendCallback.call(this,settings);
		  	}
	  		return settings;
		},
	  	onComplete: function(response, module, xhr) {
	  		devLog ('completed');
	  		if (xhr.status === 403) { //forbidden, redirect to login page
	  			location.href = kitantik.loginUrl;
	  		}
		},
		onSuccess: function(response) {
			devLog('onsuccess, success ' + response.success );
			if (response.success === true && successRedirectUrl) {
				debugger;
				devLog ("redirect " + successRedirectUrl);
				location.href = successRedirectUrl;
				return;
			}
			//TODO dolaşırken ul yap
			//$('.message').text(''); //içeriği boşalt
			
			if (showGeneralMessage) {
				var mainMessageDiv = $('.message.na-main');
				mainMessageDiv.find('div[class=header]').text ('');
				mainMessageDiv.removeClass('hidden').addClass(response.success ? 'success' : 'negative');
				mainMessageDiv.find('div[class=header]').text(response.messageList && response.messageList.length > 0 ? response.messageList[0] :'none');
			}
			
			netarti.sui.showGrowl (response.messageList[0], {failure: response.success == false});
			
			$('.field').removeClass('error'); //TODO formun içindekileri düzelt şeklinde genişlet
			if (response.fes){
				$('.message').find('div[class=header]').text ();
				var fes = response.fes;
				for (var i = 0; i < fes.length; i++) {
					var field = fes[i].field;
					$('#' + field).parent('.field').addClass('error');
				}
			}
			
			//IF message exists, windows scroll to message
			//window.scrollTo(0,0);
			
		  	if (jQuery.isFunction(onSuccessCallback)) {
				onSuccessCallback.call(this, response);
		  	}
		}
	}
};

/**
 * params = {
 * 	header: 'sss'
 * }<br>
 *
 * default options 
 *{
 * ele: 'body',
   type: 'info',
   offset: {
    from: 'top',
    amount: 20
   },
   align: 'right',
   width: 250,
   delay: 4000,
   allow_dismiss: true,
   stackup_spacing: 10
  }
 * 
 * @param message
 * @param params 
 */
netarti.sui.showGrowl = function (message, params) {
	params = params || {};
	$.semanticUiGrowl(message || 'Mesaj yok', params);
}

function Utils() {

}

Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView, context) {
        var pageTop = $(context).scrollTop();
        var pageBottom = pageTop + $(context).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    }
};

var Utils = new Utils();