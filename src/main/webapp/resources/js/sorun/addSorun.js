kitantik.sorun = {};
kitantik.sorun.add = function (params) {
	devLog ("param.resVersion:" + params.resVersion);
	$('.ui.search.sorun').find('.remove.icon').css('display', 'none');
	$('.ui.search.sorun').find('.remove.icon').on('click', function () {
		$('.ui.search.sorun').find ('.prompt').val('');
		$('#xxxid').val('');
		$('.ui.search.sorun').find('.search.icon').css('display', 'block');
		$('.ui.search.sorun').find('.remove.icon').css('display', 'none');
	});
		
	$('.ui.search.sorun').search({
		apiSettings : {
			url : BASE_URL + '/sorun/search/?q={query}'
		},
		onSelect : function(result, response) {
			//alert(result.id + " ID'li seçildi" );
			$('#xxxid').val(result.id);
			$('.ui.form').form('set values', result.payload);
			$('.ui.search.sorun').find('.search.icon').css('display', 'none');
			$('.ui.search.sorun').find('.remove.icon').css('display', 'block');
			//$('#kategori').val();

		}
	});

	netarti.sui.api({
		bs : 'form .button',
		fs : '.ui.form',
		url : BASE_URL + '/sorun/',
		onSuccessCallback : function(response) {
			devLog("onSuccessCallback: " + response);
		}
//	 		,
//	 		fields: {
//	 			kategori: {
//	 				identifier: 'kategori',
//	 				rules: [
//	 				          {
//	 				            type   : 'empty',
//	 				            prompt : 'hata---' +  '${aciklama}'
//	 				          }
//	 				        ]
//	 			}
//			}

	});	
	devLog ("kitantik.sorun.add called");
}