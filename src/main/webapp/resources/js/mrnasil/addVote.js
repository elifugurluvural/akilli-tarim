/*
 *PostCommon id si ve yeni kullanıcı oyu alır {1,0,-1} hali hazırda varsa puanı onu alır ve karşılaştırma yapar
 gelen puanla aynı ise 0 döner aksi halde yeni puanı geçer . Sonuç eksi ise thumbDown değiştirilir artı ise thumbUp
 değişitirilir.
 */
function updateButton(id, point) {
	var voteInputId = "#vote" + id;
	var existingVotePoint = $(voteInputId).val();

	if (existingVotePoint == 0) {
		calculateNewVoteTotal(id, point);
		setThumbIcon(id, point);
	} else if (existingVotePoint > 0) {
		if (point > 0) {
			setThumbIcon(id, 0);
			calculateNewVoteTotal(id, -1);
		} else if (point < 0) {
			setThumbIcon(id, -1);
			calculateNewVoteTotal(id, -2);
		}
	} else if (existingVotePoint < 0) {
		if (point > 0) {
			setThumbIcon(id, 1);
			calculateNewVoteTotal(id, 2);
		} else if (point < 0) {
			setThumbIcon(id, 0);
			calculateNewVoteTotal(id, 1);
		}
	}

}
function calculateNewVoteTotal(id, point) {
	var textButtonId = '#voteCount' + id;
	var existingCount = parseInt($(textButtonId).text());
	existingCount = existingCount + point;
	$(textButtonId).text(existingCount);
}
function setThumbIcon(id, activeThumbNumber) {
	var upThumb = "#up" + id;
	var downThumb = "#down" + id;
	var existingVoteId = "#vote" + id;
	$(upThumb).attr('class', ' thumbs up icon link icon');
	$(downThumb).attr('class', ' thumbs down icon link icon');
	$(existingVoteId).val(activeThumbNumber);
	if (activeThumbNumber == 0) {

	} else if (activeThumbNumber < 0) {
		$(downThumb).attr('class', ' red thumbs down icon link icon');
	} else if (activeThumbNumber > 0) {
		$(upThumb).attr('class', ' green thumbs up icon link icon');
	}

}
function getNewVoteValue(id, point) {
	var voteInputId = "#vote" + id;
	var existingVotePoint = $(voteInputId).val();
	if (point == existingVotePoint) {
		return 0;
	} else {
		return point;
	}
}
$('.vote.button').api({
	method : 'post',
	beforeSend : function(settings) {
		var id = $(this).data('postid');
		var voteId = $(this).data('voteid');
		var votePoint = getNewVoteValue(id,$(this).data('votepoint'));
		settings.data = JSON.stringify({
			id : voteId,
			votePoint : votePoint,
			votePostCommon : {
				"id" : id
			}
		});
		return settings;
	},
	onSuccess : function(response) {
		updateButton($(this).data('postid'), $(this).data('votepoint'));
	},
	dataType : 'json',
	beforeXHR : function(xhr) {
		xhr.setRequestHeader('Content-Type', 'application/json');
	},
});

//
// function vote(id, point,voteId) {
// var newPoint=getNewVoteValue(id,point);
// var json = {
// "id" : voteId,
// "votePoint" : newPoint,
// "votePostCommon":{"id":id}
// }
// $.ajax({
// url : BASE_URL + '/mrnasil/addVote',
// type : 'POST',
// beforeSend : function(xhr) {
// xhr.setRequestHeader("Accept", "application/json");
// xhr.setRequestHeader("Content-Type", "application/json");
// },
// data : JSON.stringify(json),
// async : true,
// dataType : 'json',
// success : function(data) {
// updateButton(id, point);
// },
// });
// return false;
// }
