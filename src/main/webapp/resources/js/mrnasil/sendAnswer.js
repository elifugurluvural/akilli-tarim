function sendAnswer(id,postTitle,loadAnswerBool) {
	var inputId = '#textArea';
	// 	var answer = $(inputId).val();
		var answer = $('#editor').trumbowyg('html');
	var json = {
		"title" : postTitle,
		"description" : answer,
		"questionId" : id
	}
	$.ajax({
		url : BASE_URL + '/mrnasil/addAnswer',
		type : 'POST',
		beforeSend : function(xhr) {
			if(answer.length<10){
				alert("Çok kısa canım ya gerçekten yollayamam");
				return false;;
			}
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		data : JSON.stringify(json),
		async : true,
		dataType : 'json',
		success : function(response) {
			netarti.sui.api.defaults().onSuccess(response);
			$('#editor').trumbowyg('empty');
			if(!loadAnswerBool){
				loadAnswers(id);
			}
// 			$('#summerNoteGrid'+id).toggle()
		},
	});
	return false;
}
