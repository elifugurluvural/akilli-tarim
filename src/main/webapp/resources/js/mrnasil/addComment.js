//görüşleri yükleyen kod sınıfı, görüşlerde değişiklik olduğunda çağrılmalı
	function loadComments(id, toogle) {
		var comments = "#comments" + id;
		if (toogle) {
			$(comments).toggle();
		}
		$(comments).load(BASE_URL + String.format('/mrnasil/loadComments/{0}', id));
	}

	function sendComment(id) {
		var inputId = '#comment' + id;
		var comment = $(inputId).val();
		var json = {
			"comment" : comment,
			"commentedObjectId" : id
		}
		$.ajax({
			url : BASE_URL + '/mrnasil/addComment',
			type : 'POST',
			beforeSend : function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			data : JSON.stringify(json),
			async : true,
			dataType : 'json',
			success : function(data) {
				netarti.sui.api.defaults().onSuccess(data);
				loadComments(id, false);
			},
		});
		return false;
	}