
$('.bookmark.button').api({
		method : 'post',
	    beforeSend: function(settings) {
	    	var id=$(this).data('answerid');
	        settings.data= JSON.stringify({
				id:id
			});
	        return settings;
	      },
		dataType : 'json',
		beforeXHR : function(xhr) {
			xhr.setRequestHeader('Content-Type', 'application/json');
		},
	}).state({
		onActivate : function() {
			$(this).state('flash text');
		},
		text : {
			inactive : ' <i class=" star icon"></i>',
			active : '<i class="yellow star icon"></i>',
		}
});