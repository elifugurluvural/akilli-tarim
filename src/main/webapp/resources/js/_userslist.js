kitantik.userslist = {
		showDropDownForMyList: function (comp, rootProductId, rootProductName) {
			rootProductName = rootProductName.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '_');
			var rl = String.format ('/sozluk/{0}_{1}?op={2}', rootProductName, rootProductId, 'le');
			if (netarti.checkActionForLogged('#listeekle-menu-' + rootProductId, rl)) {
				var listeekleMenu = $('#listeekle-menu-' + rootProductId);
				if (!listeekleMenu.hasClass('loaded')) {
					listeekleMenu.addClass ('ui loader');
					var url = BASE_URL + SECURE_URL + "/mylist?proid=" + rootProductId ;
					$.get(url, function( data ) {
						debugger;
						listeekleMenu.find('.ui.input').css('display', 'block');
						listeekleMenu.addClass('loaded');
						listeekleMenu.prepend( $(data) );
						listeekleMenu.removeClass ('ui loader');
					});
				}
			}
		},

		myListNewItem: function (event, comp) {
			if(event.keyCode == 13) { //enter ise
				var compWrapper = $(comp),
					rootProductId = compWrapper.attr('data'),
					listName = compWrapper.val();
				kitantik.userslist.addProductToList(listName, rootProductId);
				compWrapper.val('');
				$(comp).parents('.popup').popup('hide all');				
			}
		},

		/**
		 * 
		 * @param comp
		 * @param listId
		 * @param productId
		 * @param listName addProductToList icin add name üzerinden gidiyor
		 */
		changeProductListStatus: function (comp, listId, productId, listName) {
			var status = comp.checked;
			debugger;
			if (status == true || 'on' == status) {
				kitantik.userslist.addProductToList (listName, productId);
			} else {
				kitantik.userslist.removeProductFromList (listId, productId);
			}
			
		},
		
		/**
		 * add'den farklı olarak listname ile değil list id ile işlem yapılıyor
		 * @param listId
		 * @param productId
		 * @returns
		 */
		removeProductFromList: function(listId, productId) {
			if (!listId || '' == listId) {
				alert('listId boş olamaz');
				return;
			}
			
			var url = BASE_URL + SECURE_URL + "/deleteFromMylist",
				data = {proid: productId, listId: listId};
			netarti.ajax({
				url: url,
				data: data
			}, {
				successCallback: function () {
					kitantik.userslist.removeAllPopups();
				}
			});
		},

		addProductToList: function(listName, productId) {
			if (!listName || '' == listName) {
				alert('listName boş olamaz');
				return;
			}
			
			var url = BASE_URL + SECURE_URL + "/mylist",
				data = {proid: productId, listName: listName};
			netarti.ajax({
				url: url,
				data: data
			}, {
				successCallback: function () {
					kitantik.userslist.removeAllPopups();
				}
			});
		},
		
		removeAllPopups: function () {
			$('[id^=listeekle-menu-]').removeClass('loaded');
			$('[id^=listeekle-menu-] > .userlist').remove();
			devLog ('[id^=listeekle-menu-] css loaded silindi');					
		}
};
