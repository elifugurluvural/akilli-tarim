function appReady () {
	console.log('Ready. After render');
	$('.message .close').on('click', function() {
		$(this).closest('.message').transition('fade');
	});
	
	//Make all combo as semantic ui dropdown
	var dropdowns = $('.ui.dropdown');
	for ( var i = 0; i < dropdowns.length; i++) {
		var dd = $(dropdowns[i]);
		if (!dd.hasClass('na-own')) {
			dd.dropdown();
		}
	}
}
