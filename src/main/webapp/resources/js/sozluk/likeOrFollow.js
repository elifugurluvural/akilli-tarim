/*
 * 
 idsi verilen root productun takip edilip edilmediğini döner
 */
function isUserFollowing(id){
	var elementId='isFollowing'+id;
	return document.getElementById(elementId).value == "true";
}

/*
 * 
 idsi verilen productin beğenilip beğenilmediğini döner
 */
function isUserLiked(id){
	var elementId='isLiked'+id;
	return document.getElementById(elementId).value == "true";
}
/*
 * 
 actionType a göre follow veya like hareketi olduğunu anlar, bütün rootproduct için singleEntryBlock sınıfında isFollowing ve isLiked
 değişkenlerinin sonuna idleri eklenir böylelikle her butona bir unique id denk gelir ve her biri için ikişer boolean değer tanımlanır
 button onclickinde buttonun ilgili boolean değerine göre üzerindeki text değiştirililir
 
 */
function updateButton(id,actionType,buttonId){
	var text='';
	if(actionType==='follow'){
		var isFollowingElementId='isFollowing'+id;
		document.getElementById(isFollowingElementId).value = isUserFollowing(id) ? "false" : "true";
		if(isUserFollowing(id)){
			text=$('#unfollow').val();
			increaseFollowCount(id);
		}else{
			text=$('#follow').val();
			decreaseFollowCount(id);
		}
	}else 
		
		var sumValueForNewLikeCount=0;
		if(actionType==='like'){
		var isLikedElementId='isLiked'+id;
		
		document.getElementById(isLikedElementId).value = isUserLiked(id) ? "false" : "true";
		if(isUserLiked(id)){
			text=$('#unlike').val();
			sumValueForNewLikeCount=1;
			//increaseLikeCount(id);
		}else {
			text=$('#like').val();
			sumValueForNewLikeCount=-1;
			//decreaseLikeCount(id);
		}
	}
	//İçeriği setleyen komut buradır 
	setNewTextAndNewLikeCount(id,text,sumValueForNewLikeCount);
}

/*
 * @param id
 * @param text
 * @param sumValue
 * 
 * id, butonun üzerinde yeni gösterilecek text, ve +1 veya -1 değeri gönderilmeli yeni like count için
 * gelen parametlerden uygun componentlere erişir ve yeni değerlerini atar
 * */
function setNewTextAndNewLikeCount(id,text,sumValue){
	var isLikedElementId='#likeCount'+id;
	var likeId='#like'+id;
	var count=parseInt($(isLikedElementId).text());
	count=count+sumValue;
	$(likeId).text(text);
	$(isLikedElementId).text(count);
}


function getUrl(actionType,id){
	var url='';
	if(actionType==='follow'){
		url =isUserFollowing(id) ? BASE_URL +SECURE_URL+'/sozluk/activity/un'+actionType : BASE_URL +SECURE_URL+'/sozluk/activity/'+actionType
	}else if(actionType==='like'){
		url= isUserLiked(id) ? BASE_URL +SECURE_URL+'/sozluk/activity/un'+actionType : BASE_URL +SECURE_URL+'/sozluk/activity/'+actionType
	}
	return url;
}

function likeOrFollow(id,actionType,buttonId,name) {
			var json = {
				"id" : id,
				"name":name
			}
			$.ajax({
				url : getUrl(actionType,id),
				type : 'POST',
				beforeSend : function(xhr) {
					name = name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '_');
					var rl = String.format ('/sozluk/{0}_{1}?op={2}', name, id, 'li');
					if (!netarti.checkActionForLogged('#like-'+id,rl)) {
						return false;
					}
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				data : JSON.stringify(json),
				async : true,
				dataType : 'json',
				success : function(data) {
					updateButton(id,actionType,buttonId);
				},
				error: function(){
	                 console.log("like-follow post hata muhtemelen kullanıcı giriş yapmamış")
	            }
			});
}