<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	
%>
<c:set scope="application" value="${pageContext.request.userPrincipal.name}" var="username"></c:set>

<script>
	function devLog(message) {
		if (notProd) {
			console.log(message);
		}
	}
	var BASE_URL = '${contextPath}';
	var SECURE_URL = '/secure';
	var USERNAME = '${pageContext.request.userPrincipal.name}';
	devLog("Application mode:" + APPLICATION_MODE + ", version: " + version);
</script>

<script type="text/javascript" src="${contextPath}/resources/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/jquery.semantic-ui-growl.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/init.js?v=0"></script>
<script type="text/javascript" src="${contextPath}/resources/js/after-render.js?v=0"></script>
<%-- <script type="text/javascript" src="${contextPath}/resources/js/jquery.filer.js"></script> --%>
<script type="text/javascript" src="${contextPath}/resources/js/netarti.js?v=0"></script>
<script type="text/javascript" src="${contextPath}/resources/js/_userslist.js?v=0"></script>
<script src="${contextPath}/resources/js/trumbowyg.js"></script>
<link href="${contextPath}/resources/js/trumbowyg.min.css" rel="stylesheet">

<div class="ui tiny modal" id="activityModal">
	<div class="header">
		<h3 class="ui header">
			<i class="unlock alternate icon small"></i>Giriş Gerekli
		</h3>
	</div>
	<div class="content">
		<div class="ui grid">
			<img src="${contextPath}/resources/images/aciliyor.gif">
		</div>
	</div>
	<div class="actions">
		<div class="ui approve orange button">
			<i class="checkmark icon"></i> Tamam
		</div>
	</div>
</div>
