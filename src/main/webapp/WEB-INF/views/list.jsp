<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="deleteUrl" value="/material/remove"/>    
<form action="${deleteUrl}" method="POST">
      <input id="fruit" name="fruit" type="text" value=""/>
      <input type="submit" value="delete" onClick="return confirm('sure?')"/>
</form>