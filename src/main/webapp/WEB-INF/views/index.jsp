<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>


<custom:mainLayoutSuiSozluk title='${title}'>

	<div id="container" class="ui grid">
		<div class="extra content">
		<div class="ui yellow button full-width" id="kapakButton" onclick="deneme()"> <c:choose>
						<c:when test="${environment.isClosureOpen}">
			Kapağı Kapat 
	</c:when>
						<c:otherwise>
			Kapağı Aç
	</c:otherwise>
					</c:choose>
 </div>
		</div>
		<div class="ui cards">
		<div class="ui card">
	<div class="image" id="gifImage" data-value="false">
	<c:choose>
						<c:when test="${environment.isClosureOpen}">
			<img id="" src="${contextPath}/resources/images/acik.jpg">
	</c:when>
						<c:otherwise>
			<img id="" src="${contextPath}/resources/images/kapali.jpg">
	</c:otherwise>
					</c:choose>
		
	</div>


</div>
		
			<c:forEach var="plantsInBox" items="${environment.plantsInBox}">
				<c:set var="plantsInBox" value="${plantsInBox}" scope="request"></c:set>
				<jsp:include page="singlePlantCart.jsp"></jsp:include>
			</c:forEach>
		</div>


		<!-- 
		<div class="masonry-item col5">
			<button id="loadmore" value="Load More">Load More</button>
		</div>
		 -->
	</div>

	<div class="ui active centered inline loader" id="page-loading" style="display: none;"></div>
	<div style="height: 40px;" id="xxx"></div>
</custom:mainLayoutSuiSozluk>
<script type="text/javascript" src="${contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.js"></script>

<script>
function deneme(){
	var gif=$('#gifImage').attr('data-value');
	if(gif=='true'){
		  var timestamp = new Date().getTime();
		  var x='<img src="'+'http://localhost:8080/web/resources/images/aciliyor.gif?'+timestamp+'">';
		$('#gifImage').html(x)
		$('#gifImage').attr('data-value','false');	
		$('#kapakButton').html('Kapağı Kapat')

	}else{
		$('#kapakButton').html('Kapağı Aç')
		var y='<img src="'+'http://localhost:8080/web/resources/images/kapaniyor.gif?'+timestamp+'">';
		$('#gifImage').html(y)
		$('#gifImage').attr('data-value','true');			
	}

}
</script>

