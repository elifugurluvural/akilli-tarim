<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>

<custom:mainLayoutSuiSozluk title="Akıllı Tarım Giriş">
<div class="ui center aligned grid">
  <div class="column width-50">
  	<form:form action="${contextPath}/login" method="post" class="ui large form">
  	
		<div class="ui segments">
			<div class="ui segment full-width">
				<h3 class="ui header">Hesabınıza erişmek için yapınız</h3>
			</div>
			<div class="ui segment full-width padding-30">
				<div class="field">
					<div class="ui big left icon input">
						<i class="user icon"></i> 
						<input type="text" name="userName" placeholder="Kullanıcı adı" />
					</div>
				</div>
				<div class="field">
					<div class="ui big left icon input">
						<i class="lock icon"></i> 
						<input type="password" name="password" placeholder="Parola" />
					</div>
				</div>
		
				<button class="ui fluid big orange submit button" type="submit">
					<i class="Sign In icon"></i>
				</button>
				<div class="ui error message small"></div>
			</div>
		</div>		
	</form:form>

   
  </div>
  
</div>
	<script type="text/javascript" src="${contextPath}/resources/js/_login.js?v=${resVersion}"></script>
</custom:mainLayoutSuiSozluk>
