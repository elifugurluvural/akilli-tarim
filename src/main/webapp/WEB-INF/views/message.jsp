<%@page import="java.io.IOException"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Objects"%>
<%
%>
<div class="column">
	<div class="ui hidden message na-main">
		<i class="close icon"></i>
		<div class='header'></div>
	</div>
</div>
<%!@SuppressWarnings("unchecked")
	void createInfoDivs(String sessionAttribute, String divClass, JspWriter writer, HttpSession session)
			throws IOException {
		Object infoAttr = session.getAttribute(sessionAttribute);
		List<String> infoList = null;
		if (infoAttr != null) {
			infoList = (List<String>) infoAttr;
			session.removeAttribute(sessionAttribute);
			if (infoList != null || infoList.size() > 0) {
				writer.println("<div class='" + divClass + "'>");
				writer.println("<ul class='list'>");
				for (String message : infoList) {
					writer.println("<li>" + message + "</li>");
				}
				writer.println("</ul>");
				writer.println("</div>");
			}
		}
	}%>


