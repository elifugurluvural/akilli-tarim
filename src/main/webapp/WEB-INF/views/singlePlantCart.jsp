<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="ui card">
	<div class="image" id="image${plantsInBox.name}">
		<img  src="${contextPath}/resources/images/${plantsInBox.name}.png">
	</div>
	<div class="content">
		<a class="header">${plantsInBox.name}</a>
		<div class="ui divider"></div>
		<div class="meta"></div>
		<div class="description">
			<div class="ui active progress" id="progressComplete${plantsInBox.pid}">
				<div class="bar">
					<div class="progress"></div>
				</div>
				<div class="label">Yetişme Oranı</div>
			</div>
			<div class="ui divider"></div>
			<div class="ui active progress" id="progressHumidity${plantsInBox.pid}">
				<div class="bar">
					<div class="progress"></div>
				</div>
				<div class="label">Nem</div>
			</div>
			<div class="ui divider"></div>
			<div class="ui label full-width">
				<i class="thermometer half icon"></i> Sıcaklık ${environment.temperature}
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
var pid =${plantsInBox.pid};
 var humidity=${environment.humidity};
 var growsRate=${plantsInBox.growsRate};
	$('#progressComplete'+pid).progress({
		percent : growsRate
	});
	$('#progressHumidity'+pid).progress({
		percent : humidity
	});
	var imageName='${plantsInBox.name}';
	if(growsRate<33){
		var newImageName=imageName+1;
		var a='<img src="'+'http://localhost:8080/web/resources/images/'+newImageName+'.png">';
		$('#image'+imageName).html(a);
	}else if(growsRate<67 ){
		var newImageName=imageName+2;
		var a='<img src="'+'http://localhost:8080/web/resources/images/'+newImageName+'.png">';
		$('#image'+imageName).html(a);
	}else{
		var newImageName=imageName+3;
		var a='<img src="'+'http://localhost:8080/web/resources/images/'+newImageName+'.png">';
		$('#image'+imageName).html(a);
	}
	
</script>