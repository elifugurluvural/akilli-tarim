<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="list" required="true" type="java.util.Collection"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kitantikTags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<c:if test="${!empty list}">
	<c:if test="${fn:length(list) gt 1}">
<%-- 		<p class="small">Depth Size: ${fn:length(list)}</p> --%>
		<c:forEach var="child" items="${list}">
			<div class="ui segments full-width">
				<div class="ui segment full-width ">
					<h4 class="text-orange">${child.name}
						<a class="item float-right small" style="font-weight:normal; margin-left:15px;"
							href="${contextPath}/search/d?cid=${child.id}&q=&st=rootProduct">
							Tamamını incelemek için tıklayınız <i class="arrow right icon"></i>
						</a> 
						<div class="ui tiny label float-right"> ${child.count} adet ürün mevcut </div>
					</h4>
				</div>
				<div class="ui segment full-width padding-30">
					<ul class="category-listing">
						<c:forEach var="secondChild" items="${child.subCategories}">
							<li><a class="item"
								href="${contextPath}/search/d?cid=${secondChild.id}&q=&st=rootProduct">
								${secondChild.name}
								<span class="small text-lightgrey" style="margin-left:5px;">${secondChild.count}</span>
									</a></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</c:forEach>
	</c:if>

</c:if>