<%@ attribute name="list" required="true" type="java.util.Collection" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kitantikTags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!empty list}">
	<c:forEach var="child" items="${list}">
		<div class="item tab${child.depth}" data-value="${child.id}">
			<div class="ui mini orange empty circular label">
			</div>
			<a >${child.getName(locale)} <i class="dropdown icon"></i></a>
		</div>
	<%--	<kitantikTags:categoryChildren list="${child.subCategories}" /> --%>
	</c:forEach>    
</c:if>